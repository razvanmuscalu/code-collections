## Table of Contents

##### FizzBuzz


##### GraphNode & GraphService

 - lowestCommonAncestor
 - traverseByDepth & traverseByBreadth


##### StdInOut

##### Sortable & SortService

##### BracketParser

---

### Trees

 - BSTs have O(h) (h=height) worst performance for most common operations: add, remove, search, min, max, etc.
    - if tree is skewed, performance becomes O(n)
 - balanced trees guarantee O(log(n)) height 
    - so all performance for all operations becomes O(log(n))
   
| Red-Black Tree           | AVL Tree |
| ------------------------ | ------------------ |
| fast for adding/removing | fast for searching |

##### BinaryTree & BinaryTreeService

 - lowestCommonAncestor time complexity: O(n) 
    - performs  a single traversal if assuming that both keys exist in the tree.
 - lengthToNode
 - findParent  
 - traverse pre/in/post order
 - closestValue
 - height
 - getNodesAtLevel
 - traverseReverseLevelOrder

---

##### MostCommonOccurrence

##### NumberParser

---

### Sorting

#### MergeSort

 - time complexity: O(n log(n)) [more here](https://www.khanacademy.org/computing/computer-science/algorithms/merge-sort/a/analysis-of-merge-sort)
 - space complexity: O(n)
 - does *not* work **in place** because it copies more than a constant number of elements at one point in time
 - use when you need a stable sort (performs same in best/avg/worse scenarios)
 - requires more space than quick sort
 
#### QuickSort

 - time complexity [more here](https://www.khanacademy.org/computing/computer-science/algorithms/quick-sort/a/analysis-of-quicksort)
   - *best*: O(n log(n)) 
   - *average*: O(n log(n)) can be achieved by taking 3 random elements and calculating pivot as their median
   - *worst*: O(n*2) when pivot is towards smallest/largest element in list and so every recursive step sorts an array of size close to 1 and an array of size close to n-1
 - space complexity: O(log(n))
 - works **in place**
 - use when you don't need stable sort (because best/avg/worse performance differs)
 - requires less space then merge sort
 
---
 
##### K-Difference 

 - number of pairs in list whose difference is k
 
##### LongestIncreasingSequence

 - longest sequence of increasing number in a list

##### FrequencyFinder

 - finding frequency of elements in a list (using binary search)

##### NumberToEnglishWord

 - translating a number to English

---

### Sets

#### HashTable vs HashMap

HashTable      | HashMap                   |
-------------- | ------------------------- |
synchronized   | non-synchronized          |
no null keys   | allows 1 null key         |
no null values | any number of null values |
not fail-fast  | fail-fast                 |

- *fail-fast*: throwing *ConcurrentModificationException* when thread adds/removes elements while iterating 

|     | HashSet   | TreeSet   |
| --- | --------- | --------- |
| add | O(1)      | O(log(n)) |
| get | O(1)      | O(log(n)) |


### Lists

#### ArrayList vs LinkedList

|        | ArrayList           | LinkedList               |
| ------ | ------------------- | ------------------------ |
|        | index-based         | pointer-based            |
|        | fast for retrieving | fast for adding/removing |
| get    | O(1)                | O(n)                     |
| add    | O(n)                | O(1)                     |
| remove | O(n)                | O(1)                     |