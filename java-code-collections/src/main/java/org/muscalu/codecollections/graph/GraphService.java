package org.muscalu.codecollections.graph;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class GraphService {

    public GraphNode lowestCommonAncestor(GraphNode root, String n1, String n2) {
        if (root != null) {
            if (root.getLabel().equals(n1) || root.getLabel().equals(n2))
                return root;

            List<GraphNode> children = root.getChildren();

            int count = 0;
            GraphNode lowestCommonAncestor = null;

            for (GraphNode node : children) {
                GraphNode tmpNode = lowestCommonAncestor(node, n1, n2);

                if (tmpNode != null) {
                    lowestCommonAncestor = tmpNode;
                    count++;
                }
            }

            if (count == 2) {
                return root;
            }

            return lowestCommonAncestor;
        }

        return null;
    }

    public List<GraphNode> traverseByDepth(GraphNode node) {
        List<GraphNode> visitedNodes = new LinkedList<>();
        List<GraphNode> unvisitedNodes = new LinkedList<>();
        unvisitedNodes.add(node);

        while (!unvisitedNodes.isEmpty()) {
            GraphNode currNode = unvisitedNodes.remove(0);

            List<GraphNode> newNodes = currNode.getChildren()
                    .stream()
                    .filter(skipAlreadyVisitedNodes(visitedNodes))
                    .collect(toList());

            visitedNodes.add(currNode);
            unvisitedNodes.addAll(0, newNodes);
        }

        return visitedNodes;
    }

    public List<GraphNode> traverseByBreadth(GraphNode node) {
        List<GraphNode> visitedNodes = new LinkedList<>();
        List<GraphNode> unvisitedNodes = new LinkedList<>();
        unvisitedNodes.add(node);

        while (!unvisitedNodes.isEmpty()) {
            List<GraphNode> newNodes = unvisitedNodes
                    .stream()
                    .flatMap(n -> n.getChildren().stream())
                    .filter(skipAlreadyVisitedNodes(visitedNodes))
                    .collect(toList());

            visitedNodes.addAll(unvisitedNodes);
            unvisitedNodes = newNodes;
        }

        return visitedNodes;
    }

    private static Predicate<GraphNode> skipAlreadyVisitedNodes(List<GraphNode> visitedNodes) {
        return node -> !visitedNodes.contains(node);
    }

}
