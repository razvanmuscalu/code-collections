package org.muscalu.codecollections.graph;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public class GraphNode {

    private List<GraphNode> connectedNodes = new ArrayList<>();
    private String label;

    public GraphNode(@Nonnull String label) {
        this.label = label;
    }

    public void connect(List<GraphNode> nodes) {
        connectedNodes.addAll(nodes);
    }

    public String getLabel() {
        return label;
    }

    public List<GraphNode> getChildren() {
        return connectedNodes
                .stream()
                .filter(Objects::nonNull)
                .collect(toList());
    }

}
