package org.muscalu.codecollections.solar_system;

public class Main {

    public static void main(String[] args) {
        OrbitalSystem sun = new OrbitalSystem("Sun");

        OrbitalSystem earth = new OrbitalSystem("Earth", 20, 2);
        earth.add(new OrbitalSystem("Moon", 1, 1));

        sun.add(earth);
        sun.add(new OrbitalSystem("Mars", 30, 1));

        while (true) {
            sun.tick();
            earth.tick();
        }
    }
}
