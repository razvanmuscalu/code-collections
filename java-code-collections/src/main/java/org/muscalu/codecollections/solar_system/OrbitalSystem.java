package org.muscalu.codecollections.solar_system;

import java.util.ArrayList;

import static java.lang.Math.*;
import static java.lang.String.format;
import static java.lang.System.out;
import static java.lang.Thread.sleep;

public class OrbitalSystem extends GravityObject {
    private ArrayList<GravityObject> gravityObjects = new ArrayList<>();

    public OrbitalSystem(String name) {
        this(name, 0, 0);
    }

    public OrbitalSystem(String name, double distance, double speed) {
        super(name, distance, speed);
    }

    public void add(GravityObject child) {
        gravityObjects.add(child);
    }

    public void tick() {
        for (GravityObject gravityObject : gravityObjects) {
            gravityObject.setDegree(calculateDegree(gravityObject));
            gravityObject.setX(calculateX(gravityObject));
            gravityObject.setY(calculateY(gravityObject));

            out.println(format("%s: [%2.1f, %2.1f]", gravityObject.getName(), gravityObject.getX(), gravityObject.getY()));
        }

        out.println("=======================");

        try {
            sleep(1_000);
        } catch (InterruptedException e) {
            out.println(e.getMessage());
        }
    }

    private double calculateDegree(GravityObject gravityObject) {
        return gravityObject.getDegree() + gravityObject.getSpeed();
    }

    private double calculateX(GravityObject gravityObject) {
        return gravityObject.getX() + cos(gravityObject.getDegree() / 180 * PI) * gravityObject.getDistance();
    }

    private double calculateY(GravityObject gravityObject) {
        return gravityObject.getY() - sin(gravityObject.getDegree() / 180 * PI) * gravityObject.getDistance();
    }
}