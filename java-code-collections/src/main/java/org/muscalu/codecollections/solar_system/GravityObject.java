package org.muscalu.codecollections.solar_system;

public abstract class GravityObject {

    private final String name;
    private final double distance;
    private final double speed;
    private double xPosition;
    private double yPosition;
    private double degree;

    GravityObject(String name, double distance, double speed) {
        this.name = name;
        this.distance = distance;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public double getDistance() {
        return distance;
    }

    public double getSpeed() {
        return speed;
    }

    public double getX() {
        return xPosition;
    }

    public void setX(double xPosition) {
        this.xPosition = xPosition;
    }

    public double getY() {
        return yPosition;
    }

    public void setY(double yPosition) {
        this.yPosition = yPosition;
    }

    public double getDegree() {
        return degree;
    }

    public void setDegree(double degree) {
        this.degree = degree;
    }

}
