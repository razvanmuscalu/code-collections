package org.muscalu.codecollections;

import static java.lang.String.join;

import java.util.ArrayList;
import java.util.List;

class StringSplitter {

  static String split(String text, int lineLength) {
    List<String> lines = new ArrayList<>();

    for (int i = 0; i < text.length(); i += lineLength) {
      lines.add(text.substring(i, Math.min(i + lineLength, text.length())));
    }

    return join("\n", lines);
  }

}

