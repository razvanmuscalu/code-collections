package org.muscalu.codecollections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class StdInOut {

    public List<String> streamInput() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Stream<String> stream = in.lines();

        return stream.collect(toList());
    }

    public List<String> scanInput() {
        List<String> result = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        while (sc.hasNextLine()) {
            result.add(sc.nextLine());
        }

        return result;
    }

    public void printInput() {
        streamInput().forEach(System.out::println);
    }
}
