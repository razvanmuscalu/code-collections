package org.muscalu.codecollections;

public class FrequencyFinder {

    public int countOccurrence(int a[], int target) {
        return searchLastOccurrence(a, target) - searchFirstOccurrence(a, target) + 1;
    }

    public static int searchFirstOccurrence(int a[], int target) {
        int low = 0;
        int high = a.length - 1;
        int result = -1;

        while (low <= high) {
            int mid = (high + low) / 2;

            if (target == a[mid]) {
                result = mid;
                high = mid - 1;
            } else if (target > a[mid]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }

        }

        return result;

    }

    public static int searchLastOccurrence(int a[], int target) {
        int low = 0;
        int high = a.length - 1;
        int result = -1;

        while (low <= high) {
            int mid = (high + low) / 2;

            if (target == a[mid]) {
                result = mid;
                low = mid + 1;
            } else if (target > a[mid]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }

        }
        return result;
    }

}