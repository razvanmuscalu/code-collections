package org.muscalu.codecollections;

import java.util.Stack;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static java.lang.String.valueOf;
import static java.util.stream.Stream.of;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class BracketParser {

    public boolean hasBalancedBrackets(String str) {
        if (isEmpty(str))
            return true;

        Stack<String> openedBrackets = new Stack<>();
        Stack<String> closingBrackets = new Stack<>();

        str.chars()
                .mapToObj(i -> valueOf((char) i))
                .forEach(parseBracket(openedBrackets, closingBrackets));

        return (openedBrackets.size() == 0 && closingBrackets.size() == 0);
    }

    private static Consumer<String> parseBracket(Stack<String> openedBrackets, Stack<String> closingBrackets) {
        return s -> {
            if (of("{", "[", "(", "<").anyMatch(equals(s)))
                openedBrackets.push(s);
            else if (of("}", "]", ")", ">").anyMatch(equals(s)))
                if (openedBrackets.empty())
                    closingBrackets.push(s);
                else if (of("{", "[", "(", "<").anyMatch(firstIn(openedBrackets)))
                    openedBrackets.pop();
        };
    }

    private static Predicate<String> firstIn(Stack<String> openedBrackets) {
        return b -> b.equals(openedBrackets.peek());
    }

    private static Predicate<String> equals(String s) {
        return b -> b.equals(s);
    }
}