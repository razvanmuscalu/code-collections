package org.muscalu.codecollections.bst;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.MAX_VALUE;
import static java.lang.Integer.max;
import static java.lang.Math.abs;
import static java.util.Collections.emptyList;

public class BinaryTreeService {

    public int findDistance(BinaryTreeNode root, int n1, int n2) {
        int n1Distance = lengthToNode(root, n1) - 1;
        int n2Distance = lengthToNode(root, n2) - 1;

        int lowestCommonAncestor = lowestCommonAncestor(root, n1, n2).getData();
        int lowestCommonAncestorDistance = lengthToNode(root, lowestCommonAncestor) - 1;

        return (n1Distance + n2Distance) - 2 * lowestCommonAncestorDistance;
    }

    public int lengthToNode(BinaryTreeNode root, int n1) {
        if (root != null) {
            int x = 0;
            if ((root.getData() == n1) ||
                    (x = lengthToNode(root.getLeft(), n1)) > 0 ||
                    (x = lengthToNode(root.getRight(), n1)) > 0) {
                return x + 1;
            }
            return 0;
        }
        return 0;
    }

    public BinaryTreeNode lowestCommonAncestor(BinaryTreeNode root, int n1, int n2) {
        if (root != null) {
            // If either n1 or n2 matches with root's key, report
            // the presence by returning root (Note that if a key is
            // ancestor of other, then the ancestor key becomes LCA)
            if (root.getData() == n1 || root.getData() == n2) {
                return root;
            }

            // Look for keys in left and right subtrees
            BinaryTreeNode left = lowestCommonAncestor(root.getLeft(), n1, n2);
            BinaryTreeNode right = lowestCommonAncestor(root.getRight(), n1, n2);

            // If both of the above calls return Non-NULL, then one key
            // is present in once subtree and other is present in other,
            // So this node is the LCA
            if (left != null && right != null)
                return root;

            // Otherwise check if left subtree or right subtree is LCA
            if (left != null)
                return left;

            if (right != null)
                return right;
        }

        return null;
    }

    public BinaryTreeNode findParent(BinaryTreeNode root, int n1) {
        if (root != null) {
            if (root.getLeft().getData() == n1 || root.getRight().getData() == n1)
                return root;
            else {
                if (root.getData() < n1)
                    return findParent(root.getRight(), n1);
                else
                    return findParent(root.getLeft(), n1);
            }
        }

        return null;
    }


    public int closestValue(BinaryTreeNode root, int target) {
        return closest(root, target, -1, MAX_VALUE);
    }

    private int closest(BinaryTreeNode root, int target, int goal, double min) {
        if (root == null)
            return goal;

        /*
         * update minimum distance found
         * and element located at this minimum distance
         */
        if (abs(root.getData() - target) < min) {
            min = abs(root.getData() - target);
            goal = root.getData();
        }

        if (target < root.getData())
            return closest(root.getLeft(), target, goal, min);
        else
            return closest(root.getRight(), target, goal, min);

    }


    public List<BinaryTreeNode> traverseReverseLevelOrder(BinaryTreeNode node, List<BinaryTreeNode> nodes) {
        for (int i = height(node); i >= 1; i--)
            nodes.addAll(getNodesAtLevel(node, i, new ArrayList<>()));

        return nodes;
    }

    public List<BinaryTreeNode> getNodesAtLevel(BinaryTreeNode node, int level, List<BinaryTreeNode> nodes) {
        if (node == null)
            return emptyList();

        if (level == 1)
            nodes.add(node);
        else if (level > 1) {
            getNodesAtLevel(node.getLeft(), level - 1, nodes);
            getNodesAtLevel(node.getRight(), level - 1, nodes);
        }

        return nodes;
    }

    public int height(BinaryTreeNode node) {
        if (node == null)
            return 0;
        else {
            int lHeight = height(node.getLeft());
            int rHeight = height(node.getRight());

            // use the larger height
            if (lHeight > rHeight)
                return (lHeight + 1);
            else
                return (rHeight + 1);
        }
    }

    public int size(BinaryTreeNode root) {
        if (root == null)
            return 0;
        else
            return (size(root.getLeft()) + 1 + size(root.getRight()));
    }

    public boolean isBST(BinaryTreeNode node, int min, int max) {
        if (node == null)
            return true;

        if (node.getData() < min || node.getData() > max)
            return false;

        /*
         * otherwise check the subtrees recursively
         * tightening the min/max constraints
         *
         * allow only distinct values
        */
        return (isBST(node.getLeft(), min, node.getData() - 1) &&
                isBST(node.getRight(), node.getData() + 1, max));
    }

    public int largestBST(BinaryTreeNode root) {
        if (isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE))
            return size(root);
        else
            return max(largestBST(root.getLeft()), largestBST(root.getRight()));
    }


    //TODO - add tests
    public boolean isMirrored(BinaryTreeNode node) {
        return node != null && isMirrored(node.getLeft(), node.getRight());
    }

    private boolean isMirrored(BinaryTreeNode left, BinaryTreeNode right) {
        if (left == null && right == null)
            return true;

        if (left == null ^ right == null)
            return false;

        return left.getData() == right.getData()
                && isMirrored(left.getLeft(), right.getRight())
                && isMirrored(left.getRight(), right.getLeft());
    }
}
