package org.muscalu.codecollections.bst;

import java.util.List;

public class BinaryTreeNode {

    private final int data;
    private BinaryTreeNode left;
    private BinaryTreeNode right;

    public BinaryTreeNode(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public BinaryTreeNode(int data, BinaryTreeNode left, BinaryTreeNode right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public int getData() {
        return data;
    }

    public BinaryTreeNode getLeft() {
        return left;
    }

    public BinaryTreeNode getRight() {
        return right;
    }

    public void addNode(int num) {
        if (num < this.data) {
            if (this.left != null) {
                this.left.addNode(num);
            } else {
                this.left = new BinaryTreeNode(num);
            }

        } else {
            if (this.right != null) {
                this.right.addNode(num);
            } else {
                this.right = new BinaryTreeNode(num);
            }
        }
    }

    //TODO - add tests
    public BinaryTreeNode clone() {
        BinaryTreeNode left = null;
        BinaryTreeNode right = null;

        if (this.left != null) {
            left = this.left.clone();
        }

        if (this.right != null) {
            right = this.right.clone();
        }

        return new BinaryTreeNode(data, left, right);
    }

    // Visit the node first, then left and right sub-trees
    public List<BinaryTreeNode> traversePreOrder(List<BinaryTreeNode> nodes) {
        nodes.add(this);
        if (this.left != null) {
            this.left.traversePreOrder(nodes);
        }
        if (this.right != null) {
            this.right.traversePreOrder(nodes);
        }
        return nodes;
    }

    // Visit left sub-tree, then node and then, right sub-tree
    public List<BinaryTreeNode> traverseInOrder(List<BinaryTreeNode> nodes) {
        if (this.left != null) {
            this.left.traverseInOrder(nodes);
        }
        nodes.add(this);
        if (this.right != null) {
            this.right.traverseInOrder(nodes);
        }
        return nodes;
    }

    // Visit left sub-tree, then right sub-tree and then the node
    public List<BinaryTreeNode> traversePostOrder(List<BinaryTreeNode> nodes) {
        if (this.left != null) {
            this.left.traversePostOrder(nodes);
        }
        if (this.right != null) {
            this.right.traversePostOrder(nodes);
        }
        nodes.add(this);
        return nodes;
    }

}
