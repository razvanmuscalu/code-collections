package org.muscalu.codecollections.poker;

import static org.muscalu.codecollections.poker.HandUtils.isFlush;
import static org.muscalu.codecollections.poker.HandUtils.isFourOfAKind;
import static org.muscalu.codecollections.poker.HandUtils.isFullHouse;
import static org.muscalu.codecollections.poker.HandUtils.isPair;
import static org.muscalu.codecollections.poker.HandUtils.isStraight;
import static org.muscalu.codecollections.poker.HandUtils.isStraightFlush;
import static org.muscalu.codecollections.poker.HandUtils.isThreeOfAKind;
import static org.muscalu.codecollections.poker.HandUtils.isTwoPair;

import java.util.List;
import java.util.stream.Collectors;

public class Hand implements Comparable<Hand> {

  private final List<Card> hand;
  private final int[] values;
  private final String[] suits;

  public Hand(List<Card> cards) {
    hand = cards;
    values = hand.stream().mapToInt(Card::getRank).sorted().toArray();
    suits = hand.stream().map(Card::getSuit).sorted().toArray(String[]::new);
  }

  public int[] getValues() {
    return values;
  }

  public String[] getSuits() {
    return suits;
  }

  public double getRanking() {
    double ranking;

    if (isStraightFlush(values, suits) > 8.0) {
      ranking = isStraightFlush(values, suits);
    } else if (isFourOfAKind(values) > 7.0) {
      ranking = isFourOfAKind(values);
    } else if (isFullHouse(values) > 6.0) {
      ranking = isFullHouse(values);
    } else if (isFlush(values, suits) > 5.0) {
      ranking = isFlush(values, suits);
    } else if (isStraight(values) > 4.0) {
      ranking = isStraight(values);
    } else if (isThreeOfAKind(values) > 3.0) {
      ranking = isThreeOfAKind(values);
    } else if (isTwoPair(values) > 2.0) {
      ranking = isTwoPair(values);
    } else if (isPair(values) > 1.0) {
      ranking = isPair(values);
    } else {
      ranking = values[4] * 0.01;
    }

    return ranking;
  }

  public int compareTo(Hand other) {
    return Double.compare(this.getRanking(), other.getRanking());
  }

  public String toString() {
    return hand.stream().map(Card::toString).collect(Collectors.joining(","));
  }
}
