package org.muscalu.codecollections.poker;

public class Card implements Comparable<Card> {

  private final int rank;
  private final String suit;

  public Card(final int rank, final String suit) {
    this.rank = rank;
    this.suit = suit;
  }

  public static Card.CardBuilder builder() {
    return new Card.CardBuilder();
  }

  public boolean equals(Card other) {
    return this.rank == other.rank;
  }

  public int compareTo(Card other) {
    return Integer.compare(this.rank, other.rank);
  }

  public String toString() {
    return this.rank + " of " + this.suit;
  }

  public int getRank() {
    return this.rank;
  }

  public String getSuit() {
    return this.suit;
  }

  public static class CardBuilder {

    private int rank;
    private String suit;

    CardBuilder() {
    }

    public Card.CardBuilder rank(final int rank) {
      this.rank = rank;
      return this;
    }

    public Card.CardBuilder suit(final String suit) {
      this.suit = suit;
      return this;
    }

    public Card build() {
      return new Card(this.rank, this.suit);
    }

    public String toString() {
      return "Card.CardBuilder(rank=" + this.rank + ", suit=" + this.suit + ")";
    }
  }
}

