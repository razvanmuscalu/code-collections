package org.muscalu.codecollections.poker;

public class HandUtils {

  public static double isPair(int[] values) {
    double result = 0.0;

    for (int i = 0; i < values.length - 1; i++) {
      if (values[i] == values[i + 1]) {
        result = 1.0 + (values[i] * 0.01);
      }
    }

    return result;
  }

  public static double isTwoPair(int[] values) {
    double result = 0.0;
    int counter = 0;

    for (int i = 0; i < values.length - 1; i++) {
      if (values[i] == values[i + 1]) {
        counter++;
        result = 2.0 + (values[i] * 0.01);
      }
    }

    return counter == 2 ? result : 0;
  }

  public static double isThreeOfAKind(int[] values) {
    double result = 0.0;

    for (int i = 0; i < values.length - 2; i++) {
      if (values[i] == values[i + 1] && values[i] == values[i + 2]) {
        result = 3.0 + (values[i] * 0.01);
      }
    }

    return result;
  }

  public static double isStraight(int[] values) {
    double result = 0.0;
    int counter = 0;

    for (int i = 0; i < values.length - 1; i++) {
      if (values[i] == values[i + 1] - 1) {
        counter++;
        result = 4.0 + (values[i + 1] * 0.01);
      }
    }

    return counter == 4 ? result : 0.0;
  }

  public static double isFlush(int[] values, String[] suits) {
    double result = 0.0;
    int counter = 0;

    String suit = suits[0];

    for (int i = 0; i < suits.length; i++) {
      if (suits[i].equals(suit)) {
        counter++;
        result = 5.0 + (values[i] * 0.01);
      }
    }

    return counter == 5 ? result : 0.0;
  }

  // todo - scoring more complex - what happens when two full houses with same three of a kind
  public static double isFullHouse(int[] values) {
    double result = 0.0;
    boolean one = false;
    boolean two = false;
    int triple = -1;

    for (int i = 0; i < values.length - 2; i++) {
      if (values[i] == values[i + 1] && values[i] == values[i + 2]) {
        one = true;
        triple = values[i];
        result = 6.0 + (values[i] * 0.01);
      }
    }

    for (int i = 0; i < values.length - 1; i++) {
      if (values[i] == values[i + 1] && values[i] != triple) {
        two = true;
      }
    }

    return one && two ? result : 0;
  }

  public static double isFourOfAKind(int[] values) {
    double result = 0.0;

    for (int i = 0; i < values.length - 3; i++) {
      if (values[i] == values[i + 1] && values[i] == values[i + 2] && values[i] == values[i + 3]) {
        result = 7.0 + (values[i] * 0.01);
      }
    }

    return result;
  }

  public static double isStraightFlush(int[] values, String[] suits) {
    double result = 0.0;

    if (isStraight(values) > 4.0 && isFlush(values, suits) > 5.0) {
      result = 8.0 + isStraight(values) - 4.0;
    }

    return result;
  }
}
