package org.muscalu.codecollections;

import static java.util.Arrays.sort;
import static org.muscalu.codecollections.FrequencyFinder.searchFirstOccurrence;

public class KDifference {

    public int kDiff(int list[], int k) {
        int count = 0, i;
        sort(list);

        for (i = 0; i < list.length - 1; i++)
            /*
             * looks to see whether an element at k distance from current element
             * exists in sublist from current element to end of list
             *
             * it uses binary search to find if such an element exists in quickest way
             */

//            if (binarySearch(list, i + 1, list.length - 1, list[i] + k) != -1)
            if (searchFirstOccurrence(list, list[i] + k) != -1)
                count++;

        return count;
    }

    /*
     * equivalent of searchFirstOccurrence
     */
    private int binarySearch(int list[], int low, int high, int x) {
        if (high >= low) {
            int mid = (low + high) / 2;
            if (x == list[mid])
                return mid;
            if (x > list[mid])
                return binarySearch(list, (mid + 1), high, x);
            else
                return binarySearch(list, low, (mid - 1), x);
        }
        return -1;
    }
}
