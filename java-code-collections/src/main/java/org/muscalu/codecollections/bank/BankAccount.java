package org.muscalu.codecollections.bank;

public class BankAccount {

    private int balance;

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void add(int amount) {
        this.balance = this.balance + amount;
    }

    public void minus(int amount) {
        this.balance = this.balance - amount;
    }
}
