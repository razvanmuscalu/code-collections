package org.muscalu.codecollections.bank;

public class Transfer {

    private int amount;
    private User from;
    private User to;

    public Transfer(int amount, User from, User to) {
        this.amount = amount;
        this.from = from;
        this.to = to;
    }

    public int getAmount() {
        return amount;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }
}
