package org.muscalu.codecollections.bank;

import static java.lang.String.format;

public class Main {

    private static final UserService userService = new UserService();
    private static final TransferService transferService = new TransferService();

    public static void main(String[] args) {
        User john = new User("John Smi", "84 Kni", new BankAccount());
        User mark = new User("Mark Bro", "42 Mau", new BankAccount());

        System.out.println(format("%s has %d", john.getName(), john.getBankAccount().getBalance()));
        System.out.println(format("%s has %d", mark.getName(), mark.getBankAccount().getBalance()));

        // another interface to add a recipient at some point in time - when editing account
        john.addRecipient(mark);

        // interface to add user at some point in time - login phase
        userService.addUser(john);
        userService.addUser(mark);

        // interface to get active user and chosen recipient at some point in time - when starting transaction
        User from = userService.getUser("John Smi");
        User to = from.getRecipient("Mark Bro");

        // create transfer object
        Transfer t1 = new Transfer(10, from, to);

        // add transfer to service - when user confirms
        transferService.addTransfer(t1);

        // at some point when match is done
        transferService.doTransfer(t1);

        System.out.println(format("%s has %d", john.getName(), john.getBankAccount().getBalance()));
        System.out.println(format("%s has %d", mark.getName(), mark.getBankAccount().getBalance()));
    }
}
