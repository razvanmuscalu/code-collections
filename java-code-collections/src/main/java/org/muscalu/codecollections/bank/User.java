package org.muscalu.codecollections.bank;

import java.util.HashMap;
import java.util.Map;

public class User {

    private String name;
    private String address;

    private Map<String, User> recipients;

    private BankAccount bankAccount;

    public User(String name, String address, BankAccount bankAccount) {
        this.name = name;
        this.address = address;
        this.bankAccount = bankAccount;
        recipients = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void addRecipient(User user) {
        recipients.put(user.getName(), user);
    }

    public User getRecipient(String name) {
        return recipients.get(name);
    }

}
