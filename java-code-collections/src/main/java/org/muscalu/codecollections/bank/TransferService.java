package org.muscalu.codecollections.bank;

import java.util.ArrayList;
import java.util.List;

public class TransferService {

    private List<Transfer> transfers = new ArrayList<>();

    public void addTransfer(Transfer t) {
        transfers.add(t);
    }

    public void doTransfer(Transfer t) {
        t.getFrom().getBankAccount().minus(t.getAmount());
        t.getTo().getBankAccount().add(t.getAmount());

        removeTransfer(t);
    }

    private void removeTransfer(Transfer t) {
        transfers.remove(t);
    }
}
