package org.muscalu.codecollections;

import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Math.pow;
import static java.lang.String.valueOf;
import static java.util.stream.Collectors.toList;

public class NumberParser {

    /*
     * 7485 = 7 * 10 (3) + 4 * 10 (2) + 8 * 10 (1) + 5 * 10 (0)
     */
    public int parseString(String str) {

        List<Integer> parts = str.chars().mapToObj(c -> parseInt(valueOf((char) c))).collect(toList());

        int result = 0;

        for (int i = 0; i < parts.size(); i++) {
            result += (parts.get(i) * pow(10, parts.size() - i - 1));
        }

        return result;
    }
}
