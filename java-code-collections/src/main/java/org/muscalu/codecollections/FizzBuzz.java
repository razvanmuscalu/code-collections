package org.muscalu.codecollections;

import static java.lang.String.valueOf;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.rangeClosed;

public class FizzBuzz {

    public String compute() {

        return rangeClosed(1, 200)
                .mapToObj(i -> {
                    if (i % 3 == 0 && i % 5 == 0 && i % 7 == 0)
                        return "FizzBuzzBang";
                    else if (i % 7 == 0 && i % 5 == 0)
                        return "BuzzBang";
                    else if (i % 5 == 0 && i % 3 == 0)
                        return "FizzBuzz";
                    else if (i % 7 == 0)
                        return "Bang";
                    else if (i % 5 == 0)
                        return "Buzz";
                    else if (i % 3 == 0)
                        return "Fizz";
                    else
                        return valueOf(i);
                })
                .collect(joining(","));
    }
}
