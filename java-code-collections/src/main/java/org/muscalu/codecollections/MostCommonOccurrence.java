package org.muscalu.codecollections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Collections.max;
import static java.util.Comparator.comparing;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class MostCommonOccurrence {

    public String findMostCommonOccurence() {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Stream<String> stream = in.lines();

        Map<String, Long> results = stream.collect(groupingBy(identity(), counting()));

        return max(results.entrySet(), comparing(Map.Entry::getValue)).getKey();
    }

}
