package org.muscalu.codecollections.sorting;

public class MergeSort {

    public void sort(int[] list) {
        int[] tmp = new int[list.length];
        mergesort(list, tmp, 0, list.length - 1);
    }

    private void mergesort(int[] list, int[] tmp, int left, int right) {
        if (left < right) {
            int center = (left + right) / 2;
            mergesort(list, tmp, left, center); // Sort the left side of the array
            mergesort(list, tmp, center + 1, right); // Sort the right side of the array
            merge(list, tmp, left, center + 1, right); // Combine them both
        }
    }

    private void merge(int[] list, int[] tmp, int left, int right, int rightEnd) {
        int leftEnd = right - 1;
        int k = left;
        int num = rightEnd - left + 1;

        while (left <= leftEnd && right <= rightEnd)
            if (list[left] < list[right])
                tmp[k++] = list[left++];
            else
                tmp[k++] = list[right++];

        while (left <= leftEnd)    // Copy rest of first half
            tmp[k++] = list[left++];

        while (right <= rightEnd)  // Copy rest of right half
            tmp[k++] = list[right++];

        // Copy tmp back
        for (int i = 0; i < num; i++, rightEnd--)
            list[rightEnd] = tmp[rightEnd];
    }
}
