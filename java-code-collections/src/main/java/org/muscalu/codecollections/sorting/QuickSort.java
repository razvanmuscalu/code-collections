package org.muscalu.codecollections.sorting;

public class QuickSort {

    public void sort(int[] list, int low, int high) {
        if (list == null || list.length == 0)
            return;

        if (low >= high)
            return;

        int pivot = list[(low + high) / 2]; // pick the pivot
        int left = low, right = high;

        // make left < pivot and right > pivot
        while (left <= right) {
            while (list[left] < pivot)
                left++;

            while (list[right] > pivot)
                right--;

            if (left <= right) {
                int temp = list[left];
                list[left] = list[right];
                list[right] = temp;
                left++;
                right--;
            }
        }

        // recursively sort two sub parts
        if (low < right)
            sort(list, low, right);

        if (high > left)
            sort(list, left, high);
    }

}
