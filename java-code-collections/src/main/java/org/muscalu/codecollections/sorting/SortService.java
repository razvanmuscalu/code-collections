package org.muscalu.codecollections.sorting;

import java.util.Comparator;
import java.util.List;

import static java.lang.Integer.compare;
import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

public class SortService {

    public List<Sortable> sortByEmployeeNumber(List<Sortable> employees) {
        Comparator<Sortable> byEmployeeNumber = (e1, e2) -> compare(e1.getEmployeeNumber(), e2.getEmployeeNumber());

        return employees
                .stream()
                .sorted(byEmployeeNumber)
                .collect(toList());
    }

    public List<Sortable> sortByEmployeeNumberUsingInlineComparator(List<Sortable> employees) {
        return employees
                .stream()
                .sorted(comparingInt(Sortable::getEmployeeNumber))
                .collect(toList());
    }

    public List<Sortable> sortByHireDate(List<Sortable> employees) {
        return employees
                .stream()
                .sorted((e1, e2) -> e1.getHireDate().compareTo(e2.getHireDate()))
                .collect(toList());
    }

    public List<Sortable> sortByMultipleCriteria(List<Sortable> employees) {
        Comparator<Sortable> byFirstName = comparing(Sortable::getEmployeeFirstName);
        Comparator<Sortable> byLastName = (e1, e2) -> e1.getEmployeeLastName().compareTo(e2.getEmployeeLastName());

        return employees
                .stream()
                .sorted(byFirstName.thenComparing(byLastName))
                .collect(toList());
    }
}
