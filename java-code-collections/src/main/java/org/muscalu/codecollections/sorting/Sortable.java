package org.muscalu.codecollections.sorting;

import java.time.LocalDate;

public class Sortable {

    private final Integer employeeNumber;
    private final String employeeFirstName;
    private final String employeeLastName;
    private final LocalDate hireDate;

    public Sortable(Integer employeeNumber, String employeeFirstName, String employeeLastName, LocalDate hireDate) {
        this.employeeNumber = employeeNumber;
        this.employeeFirstName = employeeFirstName;
        this.employeeLastName = employeeLastName;
        this.hireDate = hireDate;
    }

    public Integer getEmployeeNumber() {
        return employeeNumber;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }
}