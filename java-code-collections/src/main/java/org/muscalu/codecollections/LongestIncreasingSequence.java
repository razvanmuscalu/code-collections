package org.muscalu.codecollections;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

public class LongestIncreasingSequence {

    public List<Integer> findLongestIncreasingSequence(int[] arr) {

        if (arr == null || arr.length == 0)
            return emptyList();

        List<Integer> lis = new ArrayList<>();

        for (int num : arr) {
            // add if first element or bigger than last element in list
            if (lis.size() == 0 || num > lis.get(lis.size() - 1)) {
                lis.add(num);
            // else replace smallest element in list, but bigger than num
            } else {
                int i = 0;
                int j = lis.size() - 1;

                while (i < j) {
                    int mid = (i + j) / 2;
                    if (lis.get(mid) < num) {
                        i = mid + 1;
                    } else {
                        j = mid;
                    }
                }

                lis.set(j, num);
            }
        }

        return lis;

    }

}
