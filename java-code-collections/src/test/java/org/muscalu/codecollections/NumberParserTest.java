package org.muscalu.codecollections;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NumberParserTest {

    public NumberParser sut = new NumberParser();

    @Test
    public void shouldParseCorrectly() {
        assertThat(sut.parseString("7485")).isEqualTo(7485);
    }
}
