package org.muscalu.codecollections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static java.lang.System.setIn;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class StdInOutTest {

    public static class SystemInTest {

        private final StdInOut sut = new StdInOut();

        private final String input = "aaa\nbbb";

        @Before
        public void setUp() {
            setIn(new ByteArrayInputStream(input.getBytes()));
        }

        @Test
        public void shouldStreamInput() {
            assertThat(sut.streamInput()).containsExactly("aaa", "bbb");
        }

        @Test
        public void shouldScanInput() {
            assertThat(sut.scanInput()).containsExactly("aaa", "bbb");
        }
    }

    public static class SystemOutRuleTest {

        private final StdInOut sut = new StdInOut();

        @Rule
        public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

        private final String input = "aaa\nbbb";

        @Before
        public void setUp() {
            setIn(new ByteArrayInputStream(input.getBytes()));
        }

        @Test
        public void shouldPrintInput() {
            sut.printInput();

            assertThat(systemOutRule.getLog()).isEqualTo("aaa\nbbb\n");
        }
    }

    public static class ByteArrayOutputStreamTest {

        private final StdInOut sut = new StdInOut();

        private final String input = "aaa\nbbb";

        private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

        @Before
        public void setUpStreams() {
            setIn(new ByteArrayInputStream(input.getBytes()));

            System.setOut(new PrintStream(outContent));
            System.setErr(new PrintStream(errContent));
        }

        @Test
        public void shouldPrintInput() {
            sut.printInput();

            assertThat(outContent.toString()).isEqualTo("aaa\nbbb\n");
        }

    }
}
