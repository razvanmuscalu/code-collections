package org.muscalu.codecollections;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LongestIncreasingSequenceTest {

    private final LongestIncreasingSequence sut = new LongestIncreasingSequence();

    @Test
    public void ShouldReturnLongestIncreasingSequence() {
        assertThat(sut.findLongestIncreasingSequence(new int[]{9, 1, 3, 7, 5, 6, 15}))           .containsExactly(1, 3, 5, 6, 15);
        assertThat(sut.findLongestIncreasingSequence(new int[]{9, 1, 3, 7, 5, 6, 15, 20}))       .containsExactly(1, 3, 5, 6, 15, 20);
        assertThat(sut.findLongestIncreasingSequence(new int[]{9, 1, 3, 7, 5, 6, 15, 20, 8}))    .containsExactly(1, 3, 5, 6, 8, 20);
        assertThat(sut.findLongestIncreasingSequence(new int[]{9, 1, 3, 7, 5, 6, 15, 20, 8, 21})).containsExactly(1, 3, 5, 6, 8, 20, 21);
    }
}
