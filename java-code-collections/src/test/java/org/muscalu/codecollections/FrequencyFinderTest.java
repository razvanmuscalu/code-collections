package org.muscalu.codecollections;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FrequencyFinderTest {

    private FrequencyFinder sut = new FrequencyFinder();

    @Test
    public void shouldFindFrequencyOfElementsInArray() {
        int b[] = {2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 10, 10, 10, 18, 18, 20, 20, 20, 20, 20};

        assertThat(sut.countOccurrence(b, 10)).isEqualTo(3);
        assertThat(sut.countOccurrence(b, 2)).isEqualTo(6);
    }
}
