package org.muscalu.codecollections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class BracketParserTest {

    private final BracketParser sut = new BracketParser();

    @Parameterized.Parameters(name = "{0} is balanced: {1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, true},
                {"", true},
                {"()", true},
                {"(", false},
                {")", false},
                {"[]", true},
                {"[", false},
                {"]", false},
                {"{}", true},
                {"{", false},
                {"}", false},
                {"<>", true},
                {"<", false},
                {">", false},
                {"[(]", false},
                {"[)]", false},
                {"<(blah{blih[bleh]bloh}bluh)>", true}
        });
    }

    private final String input;
    private final boolean result;

    public BracketParserTest(String input, boolean result) {
        this.input = input;
        this.result = result;
    }

    @Test
    public void shouldCalculateCorrectlyBalancedBrackets() {
        assertThat(sut.hasBalancedBrackets(input)).isEqualTo(result);
    }
}
