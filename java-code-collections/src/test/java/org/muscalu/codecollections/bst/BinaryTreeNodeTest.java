package org.muscalu.codecollections.bst;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Stream.of;
import static org.assertj.core.api.Assertions.assertThat;

public class BinaryTreeNodeTest {

    private final BinaryTreeNode bt = new BinaryTreeNode(5);

    private final BinaryTreeService sut = new BinaryTreeService();

    @Before
    public void setUp() {
        of(5, 6, 3, 1, 2, 4).skip(1).forEach(bt::addNode);
    }

    @Test
    public void shouldTraversePreOrder() {
        List<BinaryTreeNode> binaryTreeNodes = bt.traversePreOrder(new LinkedList<>());

        assertThat(binaryTreeNodes).extracting("data").containsExactly(5, 3, 1, 2, 4, 6);
    }

    @Test
    public void shouldTraverseInOrder() {
        List<BinaryTreeNode> binaryTreeNodes = bt.traverseInOrder(new LinkedList<>());

        assertThat(binaryTreeNodes).extracting("data").containsExactly(1, 2, 3, 4, 5, 6);
    }

    @Test
    public void shouldTraversePostOrder() {
        List<BinaryTreeNode> binaryTreeNodes = bt.traversePostOrder(new LinkedList<>());

        assertThat(binaryTreeNodes).extracting("data").containsExactly(2, 1, 4, 3, 6, 5);
    }
}
