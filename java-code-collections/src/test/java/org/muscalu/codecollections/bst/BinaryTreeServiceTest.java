package org.muscalu.codecollections.bst;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static java.util.stream.Stream.of;
import static org.assertj.core.api.Assertions.assertThat;

public class BinaryTreeServiceTest {

    private final BinaryTreeNode bt = new BinaryTreeNode(5);

    private final BinaryTreeService sut = new BinaryTreeService();

    @Before
    public void setUp() {
        of(5, 6, 3, 1, 2, 4).skip(1).forEach(bt::addNode);
    }

    @Test
    public void shouldFindDistanceBetweenTwoNodesCorrectly() {
        assertThat(sut.findDistance(bt, 2, 5)).isEqualTo(3);
        assertThat(sut.findDistance(bt, 2, 4)).isEqualTo(3);
        assertThat(sut.findDistance(bt, 1, 6)).isEqualTo(3);
    }

    @Test
    public void shouldFindDistanceToNodeCorrectly() {
        assertThat(sut.lengthToNode(bt, 3)).isEqualTo(2);
        assertThat(sut.lengthToNode(bt, 4)).isEqualTo(3);
        assertThat(sut.lengthToNode(bt, 1)).isEqualTo(3);
        assertThat(sut.lengthToNode(bt, 2)).isEqualTo(4);
    }

    @Test
    public void shouldFindParentNodeCorrectly() {
        assertThat(sut.findParent(bt, 3)).extracting("data").containsExactly(5);
        assertThat(sut.findParent(bt, 1)).extracting("data").containsExactly(3);
        assertThat(sut.findParent(bt, 4)).extracting("data").containsExactly(3);
    }

    @Test
    public void shouldFindLowestCommonAncestorOfTwoChildren() {
        assertThat(sut.lowestCommonAncestor(bt, 3, 6)).extracting("data").containsExactly(5);
    }

    @Test
    public void shouldFindLowestCommonAncestorOfChildAndGrandchild() {
        assertThat(sut.lowestCommonAncestor(bt, 1, 6)).extracting("data").containsExactly(5);
        assertThat(sut.lowestCommonAncestor(bt, 2, 4)).extracting("data").containsExactly(3);
    }

    @Test
    public void shouldFindLowestCommonAncestorWhenOneArgumentIsChildOfTheOtherArgument() {
        assertThat(sut.lowestCommonAncestor(bt, 1, 3)).extracting("data").containsExactly(3);
    }

    @Test
    public void shouldFindLowestCommonAncestorWhenOneArgumentIsGrandChildOfTheOtherArgument() {
        assertThat(sut.lowestCommonAncestor(bt, 2, 3)).extracting("data").containsExactly(3);
    }

    @Test
    public void shouldFindClosestValueCorrectly() {
        final BinaryTreeNode bt = new BinaryTreeNode(50);
        of(50, 60, 30, 10, 20, 40).skip(1).forEach(bt::addNode);

        assertThat(sut.closestValue(bt, 7)).isEqualTo(10);
        assertThat(sut.closestValue(bt, 11)).isEqualTo(10);
        assertThat(sut.closestValue(bt, 16)).isEqualTo(20);
        assertThat(sut.closestValue(bt, 30)).isEqualTo(30);
    }

    @Test
    public void shouldReturnHeight() {
        assertThat(sut.height(bt)).isEqualTo(4);
    }

    @Test
    public void shouldReturnNodesAtGivenLevel() {
        assertThat(sut.getNodesAtLevel(bt, 1, new ArrayList<>()))
                .extracting("data")
                .containsExactly(5);

        assertThat(sut.getNodesAtLevel(bt, 2, new ArrayList<>()))
                .extracting("data")
                .containsExactly(3, 6);

        assertThat(sut.getNodesAtLevel(bt, 3, new ArrayList<>()))
                .extracting("data")
                .containsExactly(1, 4);

        assertThat(sut.getNodesAtLevel(bt, 4, new ArrayList<>()))
                .extracting("data")
                .containsExactly(2);
    }

    @Test
    public void shouldPrintReverseLevelOrder() {
        ;
        assertThat(sut.traverseReverseLevelOrder(bt, new ArrayList<>()))
                .extracting("data")
                .containsExactly(2, 1, 4, 3, 6, 5);
    }
}
