package org.muscalu.codecollections;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static java.lang.System.setIn;
import static org.assertj.core.api.Assertions.assertThat;

public class MostCommonOccurrenceTest {

    private final MostCommonOccurrence sut = new MostCommonOccurrence();

    private final String input = "London\nBarcelona\nMilan\nBarcelona\nParis";

    @Before
    public void setUp() {
        setIn(new ByteArrayInputStream(input.getBytes()));
    }

    @Test
    public void shouldFindMostCommonOccurence() {
        assertThat(sut.findMostCommonOccurence()).isEqualTo("Barcelona");
    }
}
