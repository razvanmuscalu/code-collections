package org.muscalu.codecollections.graph;

import org.junit.Before;
import org.junit.Test;
import org.muscalu.codecollections.graph.GraphNode;
import org.muscalu.codecollections.graph.GraphService;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class GraphServiceTest {

    private GraphNode n0 = new GraphNode("root");
    private GraphNode n1 = new GraphNode("n1");
    private GraphNode n2 = new GraphNode("n2");
    private GraphNode n3 = new GraphNode("n3");
    private GraphNode n11 = new GraphNode("n11");
    private GraphNode n12 = new GraphNode("n12");
    private GraphNode n13 = new GraphNode("n13");
    private GraphNode n111 = new GraphNode("n111");
    private GraphNode n112 = new GraphNode("n112");
    private GraphNode n113 = new GraphNode("n113");

    private GraphNode n21 = new GraphNode("n21");
    private GraphNode n22 = new GraphNode("n22");
    private GraphNode n211 = new GraphNode("n211");
    private GraphNode n212 = new GraphNode("n212");

    private GraphService sut = new GraphService();

    @Before
    public void setUp() {
        n0.connect(asList(n1, n2, n3));
        n1.connect(asList(n11, n12, n13));
        n11.connect(asList(n111, n112, n113));
        n2.connect(asList(n21, n22));
        n21.connect(asList(n211, n212));
    }

    @Test
    public void shouldFindLowestCommonAncestorOfTwoSiblings() {
        assertThat(sut.lowestCommonAncestor(n0, "n1", "n2")).extracting("label").containsExactly("root");
    }

    @Test
    public void shouldFindLowestCommonAncestorOfTwoGrandchildren() {
        assertThat(sut.lowestCommonAncestor(n0, "n11", "n13")).extracting("label").containsExactly("n1");
    }

    @Test
    public void shouldFindLowestCommonAncestorOfChildAndGrandchild() {
        assertThat(sut.lowestCommonAncestor(n0, "n12", "n2")).extracting("label").containsExactly("root");
        assertThat(sut.lowestCommonAncestor(n0, "n112", "n12")).extracting("label").containsExactly("n1");
    }

    @Test
    public void shouldFindLowestCommonAncestorOfTwoGrandchildrenFromDifferentFamilies() {
        assertThat(sut.lowestCommonAncestor(n0, "n13", "n22")).extracting("label").containsExactly("root");
    }

    @Test
    public void shouldFindLowestCommonAncestorOfChildAndGrandgrandchild() {
        assertThat(sut.lowestCommonAncestor(n0, "n113", "n3")).extracting("label").containsExactly("root");
    }

    @Test
    public void shouldFindLowestCommonAncestorWhenOneArgumentIsChildOfTheOtherArgument() {
        assertThat(sut.lowestCommonAncestor(n0, "n1", "n11")).extracting("label").containsExactly("n1");
    }

    @Test
    public void shouldFindLowestCommonAncestorWhenOneArgumentIsGrandChildOfTheOtherArgument() {
        assertThat(sut.lowestCommonAncestor(n0, "n1", "n112")).extracting("label").containsExactly("n1");
    }

    @Test
    public void shouldTraverseNodesByDepthInCorrectOrder() {
        assertThat(sut.traverseByDepth(n0)).extracting("label").containsExactly("root", "n1", "n11", "n111", "n112", "n113", "n12", "n13", "n2", "n21", "n211", "n212", "n22", "n3");
    }

    @Test
    public void shouldTraverseNodesByBreadthInCorrectOrder() {
        assertThat(sut.traverseByBreadth(n0)).extracting("label").containsExactly("root", "n1", "n2", "n3", "n11", "n12", "n13", "n21", "n22", "n111", "n112", "n113", "n211", "n212");
    }

}
