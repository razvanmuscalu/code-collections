package org.muscalu.codecollections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class NumberToEnglishWordTest {

    private NumberToEnglishWord sut = new NumberToEnglishWord();

    @Parameterized.Parameters(name = "{0} is balanced: {1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0,"zero"},
                {1, "one"},
                {16, "sixteen"},
                {100, "one hundred"},
                {118, "one hundred eighteen"},
                {200, "two hundred"},
                {219, "two hundred nineteen"},
                {800, "eight hundred"},
                {801, "eight hundred one"},
                {1316, "one thousand three hundred sixteen"},
                {1000000, "one million"},
                {2000000, "two millions "},
                {3000200, "three millions two hundred"},
                {700000, "seven hundred thousand "},
                {9000000, "nine millions "},
                {9001000, "nine millions one thousand "},
                {123456789, "one hundred twenty three millions four hundred fifty six thousand seven hundred eighty nine"}
        });
    }

    private final int number;
    private final String word;

    public NumberToEnglishWordTest(int number, String word) {
        this.number = number;
        this.word = word;
    }

    @Test
    public void shouldCalculateCorrectlyBalancedBrackets() {
        assertThat(sut.convert(number)).isEqualTo(word);
    }
}
