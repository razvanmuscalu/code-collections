package org.muscalu.codecollections;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(Enclosed.class)
public class FizzBuzzTest {

    @RunWith(Parameterized.class)
    public static class ParameterizedTest {

        @Parameterized.Parameters(name = "should contain {1} at position {0}")
        public static Iterable<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {1, "1"},
                    {3, "Fizz"},
                    {5, "Buzz"},
                    {15, "FizzBuzz"},
                    {7, "Bang"},
                    {35, "BuzzBang"},
                    {105, "FizzBuzzBang"},
            });
        }

        private final FizzBuzz sut = new FizzBuzz();

        private final int index;
        private final String expectedString;

        public ParameterizedTest(int index, String expectedString) {
            this.index = index;
            this.expectedString = expectedString;
        }

        @Test
        public void shouldContainCorrectStringAtIndex() {
            String result = sut.compute();

            String[] parts = result.split(",");

            assertThat("should contain correct string at index", parts[index - 1], is(expectedString));
        }
    }

    public static class NonParameterizedTest {

        private final FizzBuzz sut = new FizzBuzz();

        @Test
        public void shouldContain200Elements() {
            String result = sut.compute();

            String[] parts = result.split(",");

            assertThat("should contain correct number of elements", parts.length, is(200));
        }
    }
}
