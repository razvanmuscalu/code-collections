package org.muscalu.codecollections;


import static org.assertj.core.api.Assertions.assertThat;
import static org.muscalu.codecollections.StringSplitter.split;

import org.junit.jupiter.api.Test;

class StringSplitterTest {

  @Test
  void splitWhenDivisionIsPerfect() {
    String result = split("abcd", 2);

    assertThat(result).isEqualTo("ab\ncd");
  }

  @Test
  void splitWhenDivisionIsPerfect2() {
    String result = split("abcdef", 2);

    assertThat(result).isEqualTo("ab\ncd\nef");
  }

  @Test
  void splitWhenNotEnoughCharsOnLastLine() {
    String result = split("abc", 2);

    assertThat(result).isEqualTo("ab\nc");
  }

  @Test
  void splitWhenLessCharsThanGivenLineLength() {
    String result = split("abc", 4);

    assertThat(result).isEqualTo("abc");
  }

  @Test
  void splitEmptyString() {
    String result = split("", 4);

    assertThat(result).isEqualTo("");
  }
}
