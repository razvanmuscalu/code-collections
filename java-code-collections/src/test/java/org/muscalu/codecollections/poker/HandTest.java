package org.muscalu.codecollections.poker;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class HandTest {

  static Stream<Arguments> getRankingInputProvider() {
    return Stream.of(
        // straight flush
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("H").build(),
                Card.builder().rank(3).suit("H").build(),
                Card.builder().rank(4).suit("H").build(),
                Card.builder().rank(5).suit("H").build())), 8.05),
        // four of a kind
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("S").build(),
                Card.builder().rank(2).suit("D").build(),
                Card.builder().rank(2).suit("C").build(),
                Card.builder().rank(2).suit("H").build())), 7.02),
        // full house
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(1).suit("S").build(),
                Card.builder().rank(1).suit("D").build(),
                Card.builder().rank(2).suit("C").build(),
                Card.builder().rank(2).suit("H").build())), 6.02),
        // flush
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("H").build(),
                Card.builder().rank(3).suit("H").build(),
                Card.builder().rank(4).suit("H").build(),
                Card.builder().rank(6).suit("H").build())), 5.06),
        // straight
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("S").build(),
                Card.builder().rank(3).suit("D").build(),
                Card.builder().rank(4).suit("C").build(),
                Card.builder().rank(5).suit("H").build())), 4.05),
        // three of a kind
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("S").build(),
                Card.builder().rank(2).suit("D").build(),
                Card.builder().rank(2).suit("C").build(),
                Card.builder().rank(3).suit("H").build())), 3.02),
        // two pair
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("S").build(),
                Card.builder().rank(2).suit("D").build(),
                Card.builder().rank(3).suit("C").build(),
                Card.builder().rank(3).suit("H").build())), 2.03),
        // pair
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("S").build(),
                Card.builder().rank(2).suit("D").build(),
                Card.builder().rank(3).suit("C").build(),
                Card.builder().rank(4).suit("H").build())), 1.02),
        // high card
        Arguments.of(new Hand(
            List.of(
                Card.builder().rank(1).suit("H").build(),
                Card.builder().rank(2).suit("S").build(),
                Card.builder().rank(3).suit("D").build(),
                Card.builder().rank(4).suit("C").build(),
                Card.builder().rank(6).suit("H").build())), 0.06));
  }

  @Test
  void checkValues() {
    final Hand hand = new Hand(
        List.of(
            Card.builder().rank(1).suit("H").build(),
            Card.builder().rank(3).suit("S").build(),
            Card.builder().rank(4).suit("D").build(),
            Card.builder().rank(2).suit("C").build(),
            Card.builder().rank(1).suit("H").build()));
    assertThat(hand.getValues()).containsExactlyInAnyOrder(1, 1, 2, 3, 4);
  }

  @Test
  void checkSuits() {
    final Hand hand = new Hand(
        List.of(
            Card.builder().rank(1).suit("H").build(),
            Card.builder().rank(3).suit("S").build(),
            Card.builder().rank(4).suit("D").build(),
            Card.builder().rank(2).suit("C").build(),
            Card.builder().rank(1).suit("H").build()));
    assertThat(hand.getSuits()).containsExactlyInAnyOrder("C", "D", "H", "H", "S");
  }

  @ParameterizedTest
  @MethodSource("getRankingInputProvider")
  void checkGetRanking(Hand hand, double rank) {
    assertThat(hand.getRanking()).isEqualTo(rank);
  }

  @Test
  void checkCompareEqualHands() {
    final Hand hand1 = new Hand(
        List.of(
            Card.builder().rank(1).suit("H").build(),
            Card.builder().rank(2).suit("S").build(),
            Card.builder().rank(3).suit("D").build(),
            Card.builder().rank(4).suit("C").build(),
            Card.builder().rank(5).suit("H").build()));
    final Hand hand2 = new Hand(
        List.of(
            Card.builder().rank(1).suit("H").build(),
            Card.builder().rank(2).suit("S").build(),
            Card.builder().rank(3).suit("D").build(),
            Card.builder().rank(4).suit("C").build(),
            Card.builder().rank(5).suit("H").build()));
    assertThat(hand1.compareTo(hand2)).isEqualTo(0);
  }

  @Test
  void checkCompareStandardHandsOnHighestCard() {
    final Hand hand1 = new Hand(
        List.of(
            Card.builder().rank(1).suit("H").build(),
            Card.builder().rank(3).suit("S").build(),
            Card.builder().rank(5).suit("D").build(),
            Card.builder().rank(7).suit("C").build(),
            Card.builder().rank(9).suit("H").build()));
    final Hand hand2 = new Hand(
        List.of(
            Card.builder().rank(1).suit("H").build(),
            Card.builder().rank(3).suit("S").build(),
            Card.builder().rank(5).suit("D").build(),
            Card.builder().rank(7).suit("C").build(),
            Card.builder().rank(10).suit("H").build()));
    assertThat(hand1.compareTo(hand2)).isEqualTo(-1);
  }
}
