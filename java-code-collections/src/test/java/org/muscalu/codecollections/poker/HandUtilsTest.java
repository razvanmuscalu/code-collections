package org.muscalu.codecollections.poker;

import static org.assertj.core.api.Assertions.assertThat;
import static org.muscalu.codecollections.poker.HandUtils.*;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class HandUtilsTest {

  static Stream<Arguments> isPairInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 2, 3, 4, 5}, 0.0),
        Arguments.of(new int[]{1, 1, 2, 3, 4}, 1.01),
        Arguments.of(new int[]{1, 2, 2, 3, 4}, 1.02),
        Arguments.of(new int[]{1, 2, 3, 3, 4}, 1.03),
        Arguments.of(new int[]{1, 2, 3, 4, 4}, 1.04));
  }

  static Stream<Arguments> isTwoPairInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 1, 2, 2, 3}, 2.02),
        Arguments.of(new int[]{1, 1, 2, 3, 3}, 2.03),
        Arguments.of(new int[]{1, 1, 2, 3, 4}, 0.0),
        Arguments.of(new int[]{1, 2, 2, 3, 4}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 3, 4}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 4, 4}, 0.0));
  }

  static Stream<Arguments> isThreeOfAKindInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 1, 1, 2, 3}, 3.01),
        Arguments.of(new int[]{1, 2, 2, 2, 3}, 3.02),
        Arguments.of(new int[]{1, 2, 3, 3, 3}, 3.03),
        Arguments.of(new int[]{1, 1, 2, 3, 4}, 0.0),
        Arguments.of(new int[]{1, 2, 2, 3, 4}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 3, 4}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 4, 4}, 0.0));
  }

  static Stream<Arguments> isStraightInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 2, 3, 4, 5}, 4.05),
        Arguments.of(new int[]{1, 2, 3, 4, 7}, 0.0),
        Arguments.of(new int[]{1, 3, 4, 5, 6}, 0.0));
  }

  static Stream<Arguments> isFlushInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 1, 1, 2, 3}, new String[]{"H", "H", "H", "H", "H"}, 5.03),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, new String[]{"H", "H", "H", "H", "S"}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, new String[]{"H", "H", "H", "S", "H"}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, new String[]{"H", "H", "S", "H", "H"}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, new String[]{"H", "S", "H", "H", "H"}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, new String[]{"S", "H", "H", "H", "H"}, 0.0));
  }

  static Stream<Arguments> isFullHouseInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 1, 2, 2, 3}, 0.0),
        Arguments.of(new int[]{1, 2, 2, 3, 3}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, 0.0),
        Arguments.of(new int[]{1, 2, 2, 2, 3}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 3, 3}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 2}, 6.01),
        Arguments.of(new int[]{1, 1, 2, 2, 2}, 6.02));
  }

  static Stream<Arguments> IsFourOfAKindInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 1, 1, 1, 2}, 7.01),
        Arguments.of(new int[]{1, 2, 2, 2, 2}, 7.02),
        Arguments.of(new int[]{1, 2, 2, 2, 3}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 3, 3}, 0.0),
        Arguments.of(new int[]{1, 1, 1, 2, 3}, 0.0));
  }

  static Stream<Arguments> isStraightFlushInputProvider() {
    return Stream.of(
        Arguments.of(new int[]{1, 2, 3, 4, 5}, new String[]{"H", "H", "H", "H", "H"}, 8.05),
        Arguments.of(new int[]{1, 2, 3, 4, 5}, new String[]{"H", "H", "H", "H", "S"}, 0.0),
        Arguments.of(new int[]{1, 2, 3, 4, 6}, new String[]{"H", "H", "H", "H", "H"}, 0.0));
  }

  @ParameterizedTest
  @MethodSource("isPairInputProvider")
  void checkIsPair(int[] values, double ranking) {
    assertThat(isPair(values)).isEqualTo(ranking);
  }

  @ParameterizedTest
  @MethodSource("isTwoPairInputProvider")
  void checkIsTwoPair(int[] values, double ranking) {
    assertThat(isTwoPair(values)).isEqualTo(ranking);
  }

  @ParameterizedTest
  @MethodSource("isThreeOfAKindInputProvider")
  void checkIsThreeOfAKind(int[] values, double ranking) {
    assertThat(isThreeOfAKind(values)).isEqualTo(ranking);
  }

  @ParameterizedTest
  @MethodSource("isStraightInputProvider")
  void checkIsStraight(int[] values, double ranking) {
    assertThat(isStraight(values)).isEqualTo(ranking);
  }


  @ParameterizedTest
  @MethodSource("isFlushInputProvider")
  void checkIsFlush(int[] values, String[] suits, double ranking) {
    assertThat(isFlush(values, suits)).isEqualTo(ranking);
  }

  @ParameterizedTest
  @MethodSource("isFullHouseInputProvider")
  void checkIsFullHouse(int[] values, double ranking) {
    assertThat(isFullHouse(values)).isEqualTo(ranking);
  }

  @ParameterizedTest
  @MethodSource("IsFourOfAKindInputProvider")
  void checkIsFourOfAKind(int[] values, double ranking) {
    assertThat(isFourOfAKind(values)).isEqualTo(ranking);
  }

  @ParameterizedTest
  @MethodSource("isStraightFlushInputProvider")
  void checkIsStraightFlush(int[] values, String[] suits, double ranking) {
    assertThat(isStraightFlush(values, suits)).isEqualTo(ranking);
  }
}
