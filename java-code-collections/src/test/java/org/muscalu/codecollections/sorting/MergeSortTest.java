package org.muscalu.codecollections.sorting;

import org.junit.Test;

import java.util.Random;

import static java.lang.String.format;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertTrue;

public class MergeSortTest {

    private final MergeSort sut = new MergeSort();

    private final static int SIZE = 7;
    private final static int MAX = 20;

    @Test
    public void shouldMergeSort() {
        int[] numbers = produceRandomArray();
        assertOrderIsCorrect(numbers);

    }

    @Test
    public void shouldMergeSortRepeatably() {
        for (int i = 0; i < 200; i++) {
            int[] numbers = produceRandomArray();
            assertOrderIsCorrect(numbers);
        }
    }

    private int[] produceRandomArray() {
        int[] numbers = new int[SIZE];
        Random generator = new Random();
        for (int a = 0; a < numbers.length; a++) {
            numbers[a] = generator.nextInt(MAX);
        }
        return numbers;
    }

    private void assertOrderIsCorrect(int[] numbers) {
        sut.sort(numbers);

        for (int i = 0; i < numbers.length - 1; i++) {
            if (numbers[i] > numbers[i + 1]) {
                fail(format("%d should not be larger than %d", numbers[i + 1], numbers[i]));
            }
        }
        assertTrue(true);
    }
}
