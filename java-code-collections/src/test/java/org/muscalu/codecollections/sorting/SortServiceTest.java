package org.muscalu.codecollections.sorting;

import org.junit.Before;
import org.junit.Test;
import org.muscalu.codecollections.sorting.SortService;
import org.muscalu.codecollections.sorting.Sortable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.Month.APRIL;
import static java.time.Month.DECEMBER;
import static java.time.Month.JUNE;
import static org.assertj.core.api.Assertions.assertThat;

public class SortServiceTest {

    private List<Sortable> employees = new ArrayList<>();

    private final SortService sut = new SortService();

    @Before
    public void setup() {
        employees.add(new Sortable(123, "Jack", "Johnson", LocalDate.of(1988, APRIL, 12)));
        employees.add(new Sortable(345, "Cindy", "Bower", LocalDate.of(2011, DECEMBER, 15)));
        employees.add(new Sortable(567, "Perry", "Node", LocalDate.of(2005, JUNE, 07)));
        employees.add(new Sortable(467, "Pam", "Krauss", LocalDate.of(2005, JUNE, 07)));
        employees.add(new Sortable(435, "Fred", "Shak", LocalDate.of(1988, APRIL, 17)));
        employees.add(new Sortable(678, "Ann", "Lee", LocalDate.of(2007, APRIL, 12)));
    }

    @Test
    public void shouldSortByEmployeeNumber() {
        List<Sortable> sorted = sut.sortByEmployeeNumber(employees);

        assertThat(sorted).extracting("employeeNumber").containsExactly(123, 345, 435, 467, 567, 678);
    }

    @Test
    public void shouldSortByEmployeeNumberUsingInlineComparator() {
        List<Sortable> sorted = sut.sortByEmployeeNumberUsingInlineComparator(employees);

        assertThat(sorted).extracting("employeeNumber").containsExactly(123, 345, 435, 467, 567, 678);
    }

    @Test
    public void shouldSortByHireDate() {
        List<Sortable> sorted = sut.sortByHireDate(employees);

        assertThat(sorted).extracting("employeeNumber").containsExactly(123, 435, 567, 467, 678, 345);
    }

    @Test
    public void shouldSortByMultipleCriteria() {
        List<Sortable> sorted = sut.sortByMultipleCriteria(employees);

        assertThat(sorted).extracting("employeeNumber").containsExactly(678, 345, 435, 123, 467, 567);
    }
}
