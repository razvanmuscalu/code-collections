package org.muscalu.codecollections;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class KDifferenceTest {

    private final KDifference sut = new KDifference();

    @Test
    public void shouldReturnCorrectNumberOfPairsWhoseDifferenceIsK() {
        int[] a = {0, 5, 1, 7, 4, 3, 9, 6, 8};

        assertThat(sut.kDiff(a, 2)).isEqualTo(6);
    }
}
